// npm i express mongoose

const express = require("express");
const { default: mongoose } = require("mongoose");
//Server preparation
const app = express();
const port = 3001;

// [SECTION] - MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.r5jlfd4.mongodb.net/B229_to-do?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
});
// set notification for connection success or failure

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

//[SECTION] - Schemas
// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema

const taskSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Task name is Required!"]
    },
    status: {
        type: String,
        default: "Pending"
    }
});

const userSchema = new mongoose.Schema({
    username:{
        type: String,
        required: [true, "Username is Required!"]
    },
    password:{
        type: String,
        required: [true, "Password is Required!"]
    }

});

// [SECTION] - Models
const Task = mongoose.model("Task", taskSchema);
const User = mongoose.model("User", userSchema);

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.listen(port, () => console.log(`Server running at port ${port}`));

// Creating a New Task
// Business Logic
		/*
		1. Add a functionality to check if there are duplicate tasks
			- If the task already exists in the database, we return an error
			- If the task doesn't exist in the database, we add it in the database
		2. The task data will be coming from the request's body
		3. Create a new Task object with a "name" field/property
		4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
		*/

app.post("/task", (req,res)=>{
    Task.findOne({name: req.body.name}, (err, result) => {
        
        if(result != null && result.name == req.body.name){
            return res.send("Duplicate task found");
        }else{
            let newTask = new Task({
                name: req.body.name
            });
            newTask.save((saveErr, savedTask)=>{
                if(saveErr){
                    return console.error(saveErr);
                }else{
                    return res.status(201).send("New Task Created!")
                }
            });
        }
    })
});

//Getting all task
app.get("/tasks", (req,res)=>{
    Task.find({}, (err,result)=>{
        if(err){
            return console.log(err);
        }else{
            return res.status(200).json({
                data : result
            })
        }
    })
})

// POST METHOD USERS
app.post("/signup", (req, res)=>{

try{
    User.findOne({username: req.body.username}, (err, result) =>{

        if(result != null && result.username == req.body.username){
            // return res.send("Duplicate User found");
            return res.status(409).send({status: false, message: 'Duplicate User found'});
        }else if(req.body.username == "" || req.body.password == ""){
            // return res.send("Both Username and Password should not be empty");
            return res.status(400).send({status: false, message: 'Both Username and Password should not be empty' })
        }
        else{
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            });
            newUser.save((saveErr, savedTask)=>{
                if(saveErr){
                    return console.error(saveErr);
                }else{
                    return res.status(201).send("New User Registered")
                }
            });
        }

    })
}catch(error){
    console.error(error);
    return res.status(500).send({status: false, message: 'Something broke!'});
}
});